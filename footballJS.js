    function onload(){
      

        $.ajax({
    headers: { 'X-Auth-Token': '0454eed085ce489d91803f7b8b29859d' },
    url: 'http://api.football-data.org/v1/competitions/445/teams',
    dataType: 'json',
    type: 'GET',
    }).done(function(response) {
    var dropdownContent;
    

    for( var i = 0 ; i < response.teams.length ; i++){
        var name = response.teams[i].name;
        console.log(name);
        dropdownContent+= "<tr onclick=store("+i+")><td> <img src='"+response.teams[i].crestUrl +"' width='40px' height='40px'/></td><td style='padding:15px;'>"+response.teams[i].name+"</td></tr><br>";
        dropdownContent+='<br>';
    }
    
    $( "#dropdown-content" ).append(dropdownContent);
    });
    }


    function getTeam(sub)
    {
        $.ajax({
    headers: { 'X-Auth-Token': '0454eed085ce489d91803f7b8b29859d' },
    url: 'http://api.football-data.org/v1/teams/'+sub,
    dataType: 'json',
    type: 'GET',
    }).done(function(response) {
        console.log(response);
        $("#logo").attr("src",response.crestUrl );
        logoDiv.className = "unhidden";
        $('#shortname').html(response.shortName);
        league();
    });
    }

    
    function store(name)
    {
        console.log(name);
        $.ajax({
    headers: { 'X-Auth-Token': '0454eed085ce489d91803f7b8b29859d' },
    url: 'http://api.football-data.org/v1/competitions/445/teams',
    dataType: 'json',
    type: 'GET',
    }).done(function(response) {
        console.log(response);
        var id = response.teams[name]._links.self.href;
        console.log(id);
        
        var x = id.lastIndexOf("/");
        var sub = id.substring(x+1,id.length);
        console.log(sub);
        getTeam(sub);
        
        var res = response.teams[name].name;
        console.log(res);
        localStorage.setItem("teamName",res);
        localStorage.setItem("teamId",sub);
    });
    }


    function league()
    {
        $.ajax({
    headers: { 'X-Auth-Token': '0454eed085ce489d91803f7b8b29859d' },
    url: 'http://api.football-data.org/v1/competitions/445/leagueTable',
    dataType: 'json',
    type: 'GET',
    }).done(function(response) {
    var dropdownContent;
    var fixt = null; 
    var img;
    for( var k = 0 ; k <=response.standing.length -1; k++)
        {
             img = response.standing[k].crestURI;
             fixt += '<tr><td>'+response.standing[k].position+'</td>'
                        + "<td><img src='"+img+"' width='50px' height='50px'/></td>'"
                        + '<td>'+response.standing[k].points+'</td>'
                        + '<td>'+response.standing[k].teamName+'</td>'
                        + '<td>'+response.standing[k].playedGames+'</td>'
                        + '<td>'+response.standing[k].wins+'</td>'
                        + '<td>'+response.standing[k].losses+'</td>'
                        + '<td>'+response.standing[k].draws+'</td>'
                        + '<td>'+response.standing[k].goals+'</td>'
                        + '<td>'+response.standing[k].goalDifference+'</td>'
                        + '<td>'+response.standing[k].goalsAgainst+'</td></tr><br>';
        }

        fixt = '<thead><tr><th>Position</th><th>Team Logo</th><th>Points</th><th>Teams</th>'
               +'<th>Games Played</th><th>Wins</th><th>Loses</th><th>Draw</th><th>Goals</th>'
               +'<th>Goal Difference</th><th>Goal Aganist</th></tr></thead><tbody><br>'+fixt+'</tbody>';
        
        $('#league').html(fixt);
    });
    }
    
    
     function fixtures()
    {
        $.ajax({
    headers: { 'X-Auth-Token': '0454eed085ce489d91803f7b8b29859d' },
    url: 'http://api.football-data.org/v1/competitions/445/fixtures',
    dataType: 'json',
    type: 'GET',
    }).done(function(response) {
    var dropdownContent;
    console.log(response);
    var fixt = null;
    
    for(var k = response.fixtures.length -1; k >= 0; k--)
        {
            
             fixt += '<tr><td>'+response.fixtures[k].date+'</td>'
                        + '<td>'+response.fixtures[k].matchday+'</td>'
                        + '<td>'+response.fixtures[k].homeTeamName+'</td>'
                        + '<td>'+response.fixtures[k].result.goalsHomeTeam+'</td>'
                        + '<td>'+response.fixtures[k].awayTeamName+'</td>'
                        + '<td>'+response.fixtures[k].result.goalsAwayTeam+'</td>'
                      
        }
        
        fixt = '<thead><tr><th>Date</th><th>Match Day </th><th> Home Team</th><th>Home Team Goals</th>'
               +'<th>Away Team</th><th>Away Team Goals</th></tr></thead><tbody><br>'+fixt+'</tbody>';
        
        $('#league').html(fixt);
        
    });
    }
    
    
    function lastGame()
    {
        var id = localStorage.getItem("teamId");
        console.log(id);
        var name = localStorage.getItem("teamName");
            console.log(name);
        $.ajax({
    headers: { 'X-Auth-Token': '0454eed085ce489d91803f7b8b29859d' },
    url: 'http://api.football-data.org/v1/teams/'+id+'/fixtures',
    dataType: 'json',
    type: 'GET',
    }).done(function(response) {

    var fixt;
    console.log(response);

    for(var k =response.fixtures.length -1; k >=0  ; k--)
    {
           if(response.fixtures[k].status == "FINISHED")
           {
             if(response.fixtures[k].awayTeamName ==  name)
            {
                fixt += '<tr><td><strong>'+response.fixtures[k].homeTeamName+'</strong></td><td><strong>'
                         +response.fixtures[k].result.goalsAwayTeam+'</strong></td><td></strong>'
                         +response.fixtures[k].result.goalsHomeTeam+'</strong></td></tr>';
            }
            else 
            {
                fixt += '<tr><td><strong>'+response.fixtures[k].awayTeamName+'</strong></td><td></strong>'
                         +response.fixtures[k].result.goalsHomeTeam+'</strong></td><td><strong>'
                         +response.fixtures[k].result.goalsAwayTeam+'</strong></td></tr>';
            }	
            break;
           }
    }
    
    fixt = '<thead><tr><th> Opposition</th><th>Goals</th><th>Home goals</th></tr></thead><tbody>'+fixt+'</tbody>';
        
        $('#league').html(fixt);	  
    });		
    } 

    
    function lastFive(){
    var id = localStorage.getItem("teamId");
    var name = localStorage.getItem("teamName");

        $.ajax({
    headers: { 'X-Auth-Token': '0454eed085ce489d91803f7b8b29859d' },
    url: 'http://api.football-data.org/v1/teams/'+id+'/fixtures',
    dataType: 'json',
    type: 'GET',
    }).done(function(response) {
        console.log(response);
        var fixt;
        
        for( var k =response.fixtures.length -1; k >=0  ; k--)
        {
            if(response.fixtures[k].status == "FINISHED")
               {
                 
                     var r = k
                     console.log(k);
                     break;
               }
        }		   
        
        for(var e =r  ; e >= k-4 ;  e--)
        {
                       console.log(e+" " +k)
         if(response.fixtures[e].awayTeamName ==  name)
                    {
                        fixt += '<tr><td><strong>'+response.fixtures[e].homeTeamName+'</strong></td><td><strong>'
                         +response.fixtures[e].result.goalsAwayTeam+'</strong></td><td></strong>'
                         +response.fixtures[e].result.goalsHomeTeam+'</strong></td></tr>';
                    }
                    else 
                    {
                     fixt += '<tr><td><strong>'+response.fixtures[e].awayTeamName+'</strong></td><td></strong>'
                         +response.fixtures[e].result.goalsHomeTeam+'</strong></td><td><strong>'
                         +response.fixtures[e].result.goalsAwayTeam+'</strong></td></tr>';
                    }      
        }
        
        fixt = '<thead><tr><th> Opposition</th><th>Goals</th><th>Home goals</th></tr></thead><tbody>'+fixt+'</tbody>';
        $('#league').html(fixt);      
    });    
    }
    
    
    function seasonResult(){
        var id = localStorage.getItem("teamId");
        console.log(id);
        var name = localStorage.getItem("teamName");
            console.log(name);

        $.ajax({
    headers: { 'X-Auth-Token': '0454eed085ce489d91803f7b8b29859d' },
    url: 'http://api.football-data.org/v1/teams/'+id+'/fixtures',
    dataType: 'json',
    type: 'GET',
    }).done(function(response) {

    var fixt;
    console.log(response);
    for( var k = response.fixtures.length -1 ; k >= 0 ; k--)
        {

            if(response.fixtures[k].awayTeamName ==  name)
            {
                fixt += '<tr><td><strong>'+response.fixtures[k].homeTeamName+'</strong></td><td><strong>'
                         +response.fixtures[k].result.goalsAwayTeam+'</strong></td><td></strong>'
                         +response.fixtures[k].result.goalsHomeTeam+'</strong></td></tr>';
            }
            else 
            {
                fixt += '<tr><td><strong>'+response.fixtures[k].awayTeamName+'</strong></td><td></strong>'
                         +response.fixtures[k].result.goalsHomeTeam+'</strong></td><td><strong>'
                         +response.fixtures[k].result.goalsAwayTeam+'</strong></td></tr>';
            }
            
        }
        fixt = '<thead><tr><th> Opposition</th><th>Goals</th><th>Home goals</th></tr></thead><tbody>'+fixt+'</tbody>';
        
        $('#league').html(fixt);



    });	
    }
    
    
     function teamPlayers(){
    var j = localStorage.getItem("teamId");
    var l = localStorage.getItem("teamName");

    $.ajax({
    headers: { 'X-Auth-Token': '0454eed085ce489d91803f7b8b29859d' },
    url: 'http://api.football-data.org/v1/teams/'+j+'/players',
    dataType: 'json',
    type: 'GET',
    }).done(function(response) {
        
    var fixt ;
    console.log(response);
    for( var k = 0 ; k <=response.players.length -1   ; k++)
        {
              fixt += '<tr><td>'+response.players[k].jerseyNumber+'</td>'
                        + '<td>'+response.players[k].name+'</td>'
                        + '<td>'+response.players[k].nationality+'</td>'
                        + '<td>'+response.players[k].position+'</td>'
                        + '<td>'+response.players[k].dateOfBirth+'</td></tr><br>';
        }
        
         fixt = '<thead><tr><th>Jersey Number</th><th>Name</th><th>Nationality</th><th>Position</th><th>Date of Birth</th></tr></thead><tbody>'+fixt+'</tbody>';   
        
          $('#league').html(fixt);
    });	
    }

    
        function headToHead()
        {
        
                
        }



